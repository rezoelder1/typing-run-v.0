import React, { useState, useEffect } from 'react';
import car from "../ressources/Car.svg"

class Car extends React.Component {
    constructor(props) {
        super(props);
        this.interval = null;
    }

    render(){
        return (
            <span style={{display:"flex", width:"64px", height:"40px", position: "absolute", top:"calc(50% - 16px)", left:this.props.distance, zIndex:1}}>
                <img src={car} style={{width:"100%",  height:"auto"}}/>
            </span>
        );
      }
    };
    
    export default Car;