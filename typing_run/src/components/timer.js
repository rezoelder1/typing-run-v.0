import React, { useState, useEffect } from 'react';
import "./timer.css"
const { forwardRef, useRef, useImperativeHandle } = React;

class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.interval = null;

        this.state = {
            seconds: 0,
            isActive: false
        }
    }

  toggleOn() {
    this.interval = setInterval(() => {
        this.setState({seconds : this.state.seconds+1});
    }, 1);
  }

  toggleOff(){
    clearInterval(this.interval);
  }

  reset() {
    this.setState({seconds: 0})
    this.setState({isActive : false})
    clearInterval(this.interval);
  }

  render(){
    return (
        <div className="app">
        <div className="time">
            Time : {this.state.seconds}
        </div>
        </div>
    );
  }
};

export default Timer;