import './App.css';
import React from 'react';
import Timer from './components/timer';
import raceTrack from"./ressources/track.svg";
import Car from './components/car';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.initialize = this.initialize.bind(this);
    this.handleInput = this.handleInput.bind(this);

    this.time = 0;
    this.phrase = "ksaar is the place to be !";
    this.state = {
      actual: 0,
      distance: 64,
      error: false,
      phraseDic: this.initialize(this.phrase)
    }

    this.timer = React.createRef();
  }

  handleInput(e){
    if(this.timer.current.state.seconds === 0 && e === " "){
      this.timer.current.toggleOn();
    }
    else{
      if(e === this.state.phraseDic[this.state.actual].char){
        let stateCopy = this.state;
        stateCopy.phraseDic[this.state.actual].state = true;
        stateCopy.actual +=1;
        stateCopy.distance += 1820/this.state.phraseDic.length;
  
        this.setState(stateCopy);

        if (this.state.actual === this.state.phraseDic.length){
          this.timer.current.toggleOff()
          alert("You won ! Your score is "+ this.timer.current.state.seconds)
        }
      } 
  
      else{
        this.setState({ error: true });
        setTimeout(() => {
          this.setState({ error: false });
        }, 1);

        this.timer.current.reset();  
        this.setState({
          actual: 0,
          distance: 64,
          phraseDic: this.initialize(this.phrase)
        })
      }
    }
  }

  resetGame = () =>{
    this.timer.current.reset();
    this.setState({
      actual: 0,
      distance: 64,
      phraseDic: this.initialize(this.phrase)
    })
  }

  componentDidMount(){
    document.addEventListener("keydown", (e) => this.handleInput(e.key), false);
  }

  initialize(phrase){ 
    var charList = phrase.split('');
    var result = []

    for(var i=0; i<charList.length; i++){
      let char = charList[i]
      let dic = {char: char, state: false}
      result.push(dic);
    }

    return(result)
  }

  render(){
    return (
      <div className={`App`}>
        <header className={`App-header${this.state.error ? " error" : ""}`}>
          <div style={{fontSize:"32px", backgroundColor:"grey", position:"relative", top:"128px", width:"100%", justifyContent:"center", alignContent:'center'}}>
            <b>Instructions :</b><br/>
            Press <code>Space</code> to start. Then, type the sentence on your screen as fast as possible. <br/>
            /!\ <i>Capital letters and spaces count</i> /!\
          </div>

          <div style={{position:"relative", width:"100%", top:"136px"}}>
          <img style={{position:"relative", width:"100%",  height:"auto", zIndex:0}} src={raceTrack} alt="Racetrack"/>
          <Car distance={this.state.distance+"px"}/>
          </div>

          <span style={{display:"flex", flexDirection:"row", position:'relative', top:"300px"}}>
            {this.state.phraseDic.map(charDir => {
              if(charDir.char === " "){
                return <span style={{width: "10px"}}>{charDir.char}</span>;
              }
              else {return <span className={charDir.state ? 'TextDone' : 'TextNotDone'}>{charDir.char}</span>;}
            })}
          </span>

          <Timer ref={this.timer}/>

          <button style={{position:"fixed", top:"32px", left:"32px", padding:"8px 16px", backgroundColor:"grey", borderRadius:"8px", fontFamily:"Arial", fontSize:"32px", color:"white"}} onClick={this.resetGame}>Reset Game</button>
        </header>
      </div>
    );
  }
}

export default App;
